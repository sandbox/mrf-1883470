(function($) {
  Drupal.behaviors.barefootAgegate = {
   attach: function (context, settings) {
    $("#agegate:not('.age-gate-processed')").each( function() {
      $(this).addClass('age-gate-processed');
      var modal = $(this);
      var ofage = $.cookie('gate_agecheck');
      if(!ofage) {
        //standard behavior
        $(this).agegateModal('open');
        $('#overlay-dismiss').click( function() {
          $.cookie('gate_agecheck', 1, { 
            expires: 14
          });
          modal.agegateModal('close');
          return false;
        });
      }
      else {
        $('.age-gate-processed').hide();
      }
    });
   }
  };
})(jQuery);
