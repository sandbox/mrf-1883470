(function($) {

var methods = {
  open: function( ) { 
    return this.each(function(){
      $(this).show(); 
      $('#overlay-hider').show();
    });
  },
  close: function(  ) { 
    return this.each(function(){
      $(this).hide();
      $('#overlay-hider').hide();
    }); 
  },
};


$.fn.agegateModal = function( method ) {
  if ( methods[method] ) {
    return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
  } 
  else if ( typeof method === 'object' || ! method ) {
    return methods.init.apply( this, arguments );
  } 
  else {
    $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
  }    
};
 

Drupal.behaviors.agegateModal = function(context) {
  $("div.agegate-modal:not('.agegate-modal-processed')").each( function() {
    $(this).addClass('agegate-modal-processed');
    $(this).agegateModal();
  });
};

 
})(jQuery);
