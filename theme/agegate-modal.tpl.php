<div class="overlay" id="<?php print $modal_id;?>">
  <div class="overlay-inner">
    <?php
      $language_switcher = bwe_language_selector_content();
      print render($language_switcher);
    ?>
    <h2><?php echo t("Hey good lookin'");?></h2>
    <h4><?php echo t("Are you sure you're old enough to get barefoot?"); ?></h4>
    <p><?php echo t("This site is intended for those of legal drinking age. By entering, you affirm that you are of legal drinking age in the country where this site is being accessed."); ?></p>
    <a id="overlay-dismiss" href="#"><?php echo t('DIVE IN') ?></a>
  </div>
</div>
